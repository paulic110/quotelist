QuoteList = function(element) {
	this._rows = [];
	this.companies = [];
	this.wrap = element;
	var list = this;

	//create table 
	this.table = document.createElement('table')
	this.table.className= "table table-bordered";
	// this.table.setAttribute("width", "960px");
	this.tbody = document.createElement('tbody');
	this.theader = document.createElement('thead');
	this.table.appendChild(this.theader);
	this.table.appendChild(this.tbody);

	this.countDisplay = document.createElement('span');

	//append table to the parent 
	this.wrap.appendChild(this.table);
	this.wrap.appendChild(this.countDisplay);


	// create the header by passing the name of all the columns as a list, + the heeader element
	this.createHeader(["Symbol","Name","Tick","Bid","Ask","ntChange","prtChange","Bid-Ask Average"],this.theader);

	//fixed header functionality
	$(this.table).fixedHeaderTable();

	// resizable columns functionality
  $(this.table).colResizable({
     liveDrag:true,
  });

  $(this.wrap).find(".fht-thead").width(960);
  $(this.table).css('width', '960px');

  // updating the displayed rows on scroll function
  $(this.wrap).find(".fht-tbody").scroll(function(){
    var scrollTop = $(this).scrollTop();
    $body = list.tbody;
    var check ;
   	console.log($(this));
    $(this).find('tbody tr').each(function(){
    // $this.tbody.children().each(function(){


    //if at the top of table
	    if(scrollTop  <  $(this).position().top +150 && scrollTop < 100){  
	      if(!$(this).prev('tr').length){

	      	//add two rows at the top of the screen ( two rows will be deleted from the bottom )
	        check = list.addRow(0);
	        check = list.addRow(0);
	      }
	    }
	    else 
	    	//if at the bottom of the table
	      if($(this).position().top < 2*scrollTop){
	        if(!$(this).next('tr').length){

	        	//add two rows at the bottom of the screen ( two rows will be deleted from the top )
	          check = list.addRow(1);
	          check = list.addRow(1);

	          //if there are still rows to be added, push the scroll up
	          if (check)  $(list.wrap).find('.fht-tbody').scrollTop($(list.wrap).find('.fht-tbody').scrollTop()-50);
	        }
	      }
      
       
         
    });
    //if scroll at the top, and there are still rows to be added, push the scroll down
    if(scrollTop == 0 && check){
         $(this).scrollTop(50);
     }
  });
	

  $(".fht-thead .fht-table>thead>tr>th>div").remove();

	//define the function to be triggered on a new row event
	this._newRowHandler = function(row, element){
		//create the content for a row
		element.innerHTML= "<td>"+row.SYMBOL+"</td>"+
											 "<td width='250px'>"+row.NAME+"</td>"+
											 "<td width='100px' class='"+row.TICK+"'></td>"+
											 "<td width='100px'>"+row.BID+"</td>"+
											 "<td width='100px'>"+row.ASK+"</td>"+
											 "<td width='100px'>"+row.NETCHNG+"</td>"+
											 "<td width='100px'>"+row.PCTCHNG+"</td>"+
											 "<td width='100px'>"+Math.trunc((Number(row.BID)+Number(row.ASK))/2)+"</td>";
		//increase the current height of the table content
		this.showHt = this.showHt + element.clientHeight;
	}

	//define the function to be triggered on a row update event
	this._rowUpdatedHandler = function(id, field, newValue){
		//check if the updated row is found within the currently displayed rows
		if(id >= this.currentRowsS && id< this.currentRowsE) {
			var values = this.companies[id].getElementsByTagName('td');
			//update the row object (not the one displayed)
			this._rows[id][field] = newValue;

			//update the row display
			switch(field) {
				case "TICK":
					$(values[2]).fadeOut(150).fadeIn(150);
					values[2].className = newValue;
					break;
				case "BID":
					$(values[3]).fadeOut(150).fadeIn(150);
					values[3].innerHTML = newValue;
					values[7].innerHTML = Math.trunc((Number(this._rows[id][field]) + Number(this._rows[id]["ASK"]))/2); 
					break;
				case "ASK":
					$(values[4]).fadeOut(150).fadeIn(150);
					values[4].innerHTML = newValue;
					values[7].innerHTML = Math.trunc((Number(this._rows[id][field]) + Number(this._rows[id]["BID"]))/2); 
					break;
				case "NETCHNG":
					$(values[5]).fadeOut(150).fadeIn(150);
					values[5].innerHTML = newValue;
					break;
				case "PCTCHNG":
					$(values[6]).fadeOut(150).fadeIn(150);
					values[6].innerHTML = newValue;
					break;
			}
		}
		//if the row is not within thos already on display, only the row object is updated
		else {
			switch(field){
				case "TICK":
					this._rows[id].TICK = newValue;
					break;
				case "BID":
					this._rows[id].BID = newValue;
					break;
				case "ASK":
					this._rows[id].ASK = newValue;
					break;
				case "NETCHNG":
					this._rows[id].NETCHNG = newValue;
					break;
				case "PCTCHNG": 
					this._rows[id].PCTCHNG = newValue;
					break;
			}
		}
	}
}

QuoteList.prototype.wrap = null;
QuoteList.prototype.currentRowsS= 0;
QuoteList.prototype.currentRowsE= 0;
QuoteList.prototype.maxCompanies = 0;
QuoteList.prototype.showHt = 0;
QuoteList.prototype.table = null;
QuoteList.prototype.theader = null;
QuoteList.prototype.tbody = null;
QuoteList.prototype._randomCompanyIndex = 100;
QuoteList.prototype._rows = null;
QuoteList.prototype.companies = null;
QuoteList.prototype._newRowHandler = null;
QuoteList.prototype._rowUpdatedHandler = null;
QuoteList.prototype._updaterTimer = null;
QuoteList.prototype._updateRate = 500;


/*function which is called when a new row needs to be displayed
as a result of reaching the bottom of the table*/
QuoteList.prototype.addRow = function( top ){
	if(top ==1){
		//if there are rows available which are not yet displayed
		if(this.currentRowsE < this.maxCompanies){

			//fetch the row with the stored data 
			var row = this._rows[this.currentRowsE];
			this.tbody.appendChild(this.companies[this.currentRowsE]);
			this.companies[this.currentRowsS] = this.tbody.removeChild(this.companies[this.currentRowsS]);

			//add the row to the table 
			this._newRowHandler(row, this.companies[this.currentRowsE]);

			//increse the count of rows which are displayed
			this.currentRowsE ++;
			this.currentRowsS ++;
			this.countDisplay.innerHTML = this.currentRowsS + "-" + this.currentRowsE +"/" + this.maxCompanies;
			return 1;
		}
		return 0;
	}
	else if (top == 0){
		if( this.currentRowsS > 0){
			//remove the bottom row from display
			this.companies[this.currentRowsE-1] =this.tbody.removeChild(this.companies[this.currentRowsE-1]);
			this.currentRowsE--;
			
			//append the previous row at the top of the table
			$(this.tbody).prepend(this.companies[this.currentRowsS-1]);
			this.currentRowsS--;
			
			//update displayed interval idicator
			this.countDisplay.innerHTML = this.currentRowsS + "-" + this.currentRowsE +"/" + this.maxCompanies;
			return 1;
			
		}
		else {
			return 0;
		}
	}
}

// function to be called at initialisation to create the header of the table
QuoteList.prototype.createHeader = function (list, element){
	var fields = [];
  var row = document.createElement('tr');

  //create all td elements and append them to a row
  for(var i = 0; i<list.length; i++){
 		fields[i] = document.createElement('th');
 		fields[i].innerHTML = list[i];
 		fields[i].setAttribute("width","100px")
 		row.appendChild(fields[i]);
 }

 //set the width of the name column to 250px
 fields[1].setAttribute("width", "250px");

 element.appendChild(row)

}


QuoteList.prototype.createRandomSymbols = function(number) {
	var output = [];
	for (var i=0;i<number;i++) {
		output.push(btoa(""+this._randomCompanyIndex));
		this._randomCompanyIndex += 5;
	}
	return output;
}

QuoteList.prototype.randomValue = function(min, max) {
	return (Math.random() * (max - min) + min).toFixed(2);
}

QuoteList.prototype.randomIntValue = function(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

QuoteList.prototype.createRow = function(symbol) {
	return {
      SYMBOL: symbol,
      NAME: QuoteList.RANDOM_NAME[this.randomIntValue(0, QuoteList.RANDOM_NAME.length-1)],
      TICK: (this.randomIntValue(0,1) == 0)?'U':'D',
      BID: this.randomValue(0,100),
      ASK: this.randomValue(0,100),
      NETCHNG: this.randomValue(0,10),
      PCTCHNG: this.randomValue(0,100)
	}
}

QuoteList.prototype.addCompanies = function(symbolList) {
	this.maxCompanies = symbolList.length;
	setTimeout(function() {
		for (var i=0;i<symbolList.length;i++) {
			if (this._newRowHandler) {
				
				//create new row object and company
				var newRow = this.createRow(symbolList[i]);

				//add the new row and company to the existing list
				this._rows.push(newRow);
				var newCompany = document.createElement('tr');
				this.companies.push(newCompany);

				//update cerate the row in the table with the information provided
				if(this.showHt < this.wrap.clientHeight *2 ){
					this.tbody.appendChild(newCompany);
					this.currentRowsE ++;
					this._newRowHandler(newRow, newCompany);
				}
			}
			
			if (!this._updaterTimer) {
				this._updaterTimer = setInterval(function() {
					if (this._rowUpdatedHandler) {
						var updatedRowIndex = this.randomIntValue(0, this._rows.length-1);					
						var row = this._rows[updatedRowIndex];
						var updatedFieldIndex = this.randomIntValue(0,4);
						switch (updatedFieldIndex) {
							case 0:
								this._rowUpdatedHandler(updatedRowIndex, "TICK", (this.randomIntValue(0,1) == 0)?'U':'D');
								break;
							case 1:
								this._rowUpdatedHandler(updatedRowIndex, "BID", this.randomValue(0,100));
								break;
							case 2:
								this._rowUpdatedHandler(updatedRowIndex, "ASK", this.randomValue(0,100));
								break;
							case 3:
								this._rowUpdatedHandler(updatedRowIndex, "NETCHNG", this.randomValue(0,10));
								break;
							case 4:
								this._rowUpdatedHandler(updatedRowIndex, "PCTCHNG", this.randomValue(0,100));
								break;
						}
					}
				}.bind(this),this.randomIntValue(10,this._updateRate));
			}
		}
	}.bind(this),1000);
}

QuoteList.prototype.onNewRow = function(handler) {
	this._newRowHandler = handler;
}

QuoteList.prototype.onRowUpdated = function(handler) {
	this._rowUpdatedHandler = handler;
}

QuoteList.RANDOM_NAME = [
	"Microsoft",
	"Apple",
	"Sonny",
	"Samsung",
	"NULLA FACILISI. SED NEQUE. SED",
	"ORCI SEM EGET MASSA.",
	"DOLOR ELIT PELLENTESQUE A FACI",
	"FUSCE DOLOR QUAM ELEMENTUM AT",
	"A FELIS",
	"VITAE ALIQUET NEC IMPERDIET NE",
	"HABITANT MORBI TRISTIQUE SENEC",
	"QUAM. CURABITUR VEL LECTUS. CU",
	"TELLUS JUSTO SIT AMET",
	"NASCETUR RIDICULUS MUS. PROIN",
	"DOLOR",
	"CONSECTETUER IPSUM NUNC ID ENI",
	"NON ENIM COMMODO HENDRERIT. DO",
	"QUIS URNA. NUNC QUIS ARCU VEL",
	"PLACERAT AUGUE.",
	"NEC MAURIS BLANDIT MATTIS. CRA",
	"ERAT NEQUE NON QUAM. PELLENTES",
	"MOLLIS DUI IN",
	"JUSTO. PROIN NON MASSA NON ANT",
	"LACUS VESTIBULUM",
	"FUSCE DOLOR QUAM ELEMENTUM AT",
	"MUS. PROIN VEL ARCU",
	"METUS. ALIQUAM ERAT VOLUTPAT.",
	"CONGUE TURPIS. IN CONDIMENTUM.",
	"VEL EST TEMPOR BIBENDUM. DONEC",
	"MORBI TRISTIQUE SENECTUS ET NE",
	"DUI QUIS ACCUMSAN CONVALLIS AN",
	"ELIT SED CONSEQUAT AUCTOR NUNC",
	"MAURIS IPSUM",
	"DAPIBUS",
	"ORCI ADIPISCING NON LUCTUS SIT",
	"VITAE ALIQUAM EROS TURPIS NON",
	"MAURIS VESTIBULUM NEQUE SED DI",
	"AUCTOR QUIS TRISTIQUE AC ELEIF",
	"AENEAN EUISMOD MAURIS EU ELIT.",
	"ID ERAT. ETIAM VESTIBULUM MASS",
	"RISUS. DONEC EGESTAS. ALIQUAM",
	"AENEAN EGET MAGNA. SUSPENDISSE",
	"ID ANTE DICTUM CURSUS. NUNC",
	"NEC LIGULA CONSECTETUER RHONCU",
	"FACILISIS FACILISIS MAGNA TELL",
	"SAGITTIS FELIS. DONEC TEMPOR E",
	"GRAVIDA MAURIS UT MI. DUIS",
	"AUGUE PORTTITOR INTERDUM. SED",
	"DAPIBUS QUAM QUIS DIAM. PELLEN",
	"ANTE. VIVAMUS NON LOREM VITAE",
	"SOCIIS NATOQUE PENATIBUS ET MA",
	"PROIN SED TURPIS",
	"ADIPISCING LIGULA. AENEAN GRAV",
	"ODIO. ETIAM LIGULA",
	"IPSUM PORTA ELIT A FEUGIAT TEL",
	"MAURIS UT QUAM",
	"PLACERAT EGET VENENATIS A MAGN",
	"AC ARCU. NUNC MAURIS. MORBI NO",
	"EU DOLOR EGESTAS RHONCUS. PROI",
	"NULLA ALIQUET. PROIN VELIT. SE",
	"TINCIDUNT DUI AUGUE EU TELLUS.",
	"LOBORTIS NISI",
	"RISUS. DONEC EGESTAS. DUIS",
	"MOLLIS NEC CURSUS A",
	"DIAM. PROIN DOLOR. NULLA SEMPE",
	"AC TELLUS.",
	"TELLUS LOREM",
	"MASSA. QUISQUE PORTTITOR EROS",
	"DONEC NON JUSTO. PROIN",
	"NON LOBORTIS QUIS PEDE. SUSPEN",
	"SOCIIS NATOQUE PENATIBUS ET MA",
	"POSUERE CUBILIA CURAE; DONEC T",
	"VIVERRA. DONEC",
	"A FEUGIAT TELLUS",
	"PURUS IN MOLESTIE TORTOR NIBH",
	"MAGNA. CRAS CONVALLIS CONVALLI",
	"INTEGER",
	"AMET LUCTUS VULPUTATE NISI SEM",
	"MI FELIS ADIPISCING FRINGILLA",
	"TINCIDUNT ORCI QUIS LECTUS. NU",
	"ULLAMCORPER NISL ARCU IACULIS",
	"NEC ENIM. NUNC UT",
	"DUI QUIS ACCUMSAN CONVALLIS AN",
	"VEHICULA. PELLENTESQUE TINCIDU",
	"RUTRUM URNA NEC",
	"SED EST. NUNC LAOREET LECTUS Q",
	"NAM AC NULLA. IN TINCIDUNT CON",
	"FAUCIBUS LECTUS A SOLLICITUDIN",
	"CURABITUR",
	"EGESTAS BLANDIT. NAM NULLA MAG",
	"CURSUS VESTIBULUM.",
	"LOREM",
	"QUISQUE PORTTITOR EROS NEC TEL",
	"LIBERO. PROIN SED TURPIS NEC M",
	"SAPIEN. NUNC PULVINAR ARCU ET",
	"AUCTOR ODIO",
	"CONVALLIS ANTE LECTUS CONVALLI",
	"MOLESTIE",
	"EGET VOLUTPAT ORNARE FACILISIS",
	"IPSUM LEO ELEMENTUM SEM VITAE",
];
